# About #

##### BitBucket:
https://bitbucket.org/szymonsitko/pgs-tech-challenge

Sample solution for the tech-challenge (given by PGS Software). Essentialy, idea is to build the whole project using ReactJS framework (since it's so cool). All tech-stack will be described in following section.

# How to Run

In order to run & try application, you need to have NodeJS and NPM installed. Project is supplied with package.json file, so you need to navigate into project directory and issue command:

    npm install

(to install all dependencies listed in file)

and then:

    npm start

It will spin-up the development server which will 'deploy' application under http://localhost:8080 address.

# Tech Stack

There is not much. Looking at the package.json file, we are really only using

## ReactJS

It was relatively easy choice for me, since I work a lot with React-Native, which gives amazing opportunity for creation native-speaking applications for Android as well as iOS at the same time (well, sometimes just in theory).

## Axios
For the web GET request, just because it can deliver promise and this is exactly what is needed for better end user experience and for what it's expected.

# Architecture

The task was to create simple web app, that doesn't make any http requests on navigation (as the classic web apps do).
During the design phase of the project, I decided to make it extremely simple, so it won't even employ any routing
mechanism, but just switch between components, what is orchestrated from the main application container.

Because of the great ReactJS component lifecycle methods, it was relatively easy to achieve, managing the GET request,
and all other assets at the same time. In order to make application and its component re-usable, I was trying to keep well naming (verbose) standards.

### Szymon Sitko
##### A.D. 2017
