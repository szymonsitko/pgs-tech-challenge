import validator from 'email-validator';

// Validating email is super easy thanks to the third party library
// available for the js, however validating name is easier, just waiting
// for something rather than nothing. 
export function formInputsValidator(name, email) {
  if (name.length > 0) {
    if (validator.validate(email)) {
      return true;
    }
  }
  return false;
}
