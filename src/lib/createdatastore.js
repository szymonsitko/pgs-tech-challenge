export function createDataStore(source, firstName, secondName) {
  // Looping through the object received. Here, we take in account case,
  // when some of the object might be undefined so it could crash whole
  // job (btw, nobody whants that!)
  let arrayOfCamRecords = [];
  for (let i = 1; i < Object.keys(source).length; i++) {
    const recordName = source[i];
    if (!("undefined" === typeof recordName)) {
      // Function is re-usable in meaning, that when there are different locations
      // needed, it is easy to change name storeg (in this case) in constants.js
      // file, but it could come from anywhere
      if (recordName.name === firstName || recordName.name === secondName) {
        arrayOfCamRecords.push(recordName);
      }
    }
  }
  return arrayOfCamRecords;
}
