// Simple date formatted created on base of Date.now() js function,
// it always returns data in human-like format thanks to below oneliner
export function generateDatetimeStamp(dateString) {
  const newDate = new Date(dateString).toLocaleString().split(',')[0].replace(/\//g, '-');
  return newDate;
}
