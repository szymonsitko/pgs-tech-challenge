import React from 'react';
import Frame from './frame';

const Frames = props => {
  return (
    <div className="about-frames">
    {
      props.dataSource.map((details) => {
        return (
          <Frame
            key={details.title}
            uri={details.uri}
            title={details.title}
            content={details.content}
          />
        )
      })
    }
    </div>
  )
}

export default Frames;
