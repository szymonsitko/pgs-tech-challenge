import React, { Component } from 'react';
import Logo from './logo';

class NavBar extends Component {
  constructor(props) {
    super(props);

    this.state = {
      activeChild: 'about'
  };
}

  onUserClick(e) {
    const { id } = e.currentTarget;
    this.setState({ activeChild: id });
    this.props.onToggleComponent(id);
    e.preventDefault();
  }

  render() {
    return (
      <div className="navbar-container">
        <Logo />
        <ul className="navbar-list">
          <li>
            <a
              href="#"
              onClick={(e) => this.onUserClick(e)}
              className={this.state.activeChild === 'about' ? 'navbar-child-content-active' : 'navbar-child-content'}
              id="about"
            >
              ABOUT US
            </a>
          </li>
          <li className="navbar-child">
            <a
              href="#"
              onClick={(e) => this.onUserClick(e)}
              className={this.state.activeChild === 'skicams' ? 'navbar-child-content-active' : 'navbar-child-content'}
              id="skicams"
            >
              SKICAMS
            </a>
          </li>
          <li className="navbar-child">
            <a
              href="#"
              onClick={(e) => this.onUserClick(e)}
              className={this.state.activeChild === 'contact' ? 'navbar-child-content-active' : 'navbar-child-content'}
              id="contact"
            >
              CONTACT
            </a>
          </li>
        </ul>
      </div>
    );
  }
}

export default NavBar;
