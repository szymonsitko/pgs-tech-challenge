import React, { Component } from 'react';

class Logo extends Component {
  render() {
    return (
      <div className="logo-container">
        <img className="company-logo" src={'../../../assets/images/logo.png'} />
      </div>
    );
  }
}

export default Logo;
