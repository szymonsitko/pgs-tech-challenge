import React, { Component } from 'react';
import { formInputsValidator } from '../../lib/formvalidator';

class Form extends Component {
  constructor(props) {
    super(props);

    this.state = {
      name: '',
      email: '',
      message: ''
    }
  }

  onNameInputChange(event) {
    this.setState({ name: event.target.value });
  }

  onEmailInputChange(event) {
    this.setState({ email: event.target.value });
  }

  onMessageInputChange(message) {
    this.setState({ message: event.target.value });
  }

  handleSubmit(e) {
    e.preventDefault();
    const { name, email, message } = this.state;
    if (!formInputsValidator(name, email)) {
      this.setState({ formInvalid: true });
      setTimeout(() => {
        this.setState({ formInvalid: false });
      }, 2000);
    }
  }

  renderFormInvalidMessage() {
    if (this.state.formInvalid) {
      return <p style={{ color: 'red'}}>Input is not valid, please try again!</p>;
    }
  }

  render() {
    return (
      <div className="form-container">
        <form onSubmit={this.handleSubmit.bind(this)}>
          <input
            placeholder="Name*"
            type="text"
            value={this.state.name}
            onChange={this.onNameInputChange.bind(this)}
          />
          <input
            placeholder="Email*"
            type="text"
            value={this.state.email}
            onChange={this.onEmailInputChange.bind(this)}
          />
          <br />
          {this.renderFormInvalidMessage()}
          <input
            placeholder="Message"
            type="text"
            value={this.state.message}
            onChange={this.onMessageInputChange.bind(this)}
          />
          <br />
          <input id="send-btn" type="submit" value="SEND" />
        </form>
      </div>
    );
  }
}

export default Form;
