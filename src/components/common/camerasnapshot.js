import React from 'react';
import { generateDatetimeStamp } from '../../lib/dateformatter';

const CameraSnapshot = props => {
  const formattedDate = generateDatetimeStamp(Date.now());
  return (
    <div className="snapshot-container">
      <div className="snapshot-header">
        <p>{formattedDate}</p>
        <p>{props.name}</p>
      </div>
      <div>
        <img src={props.snapshots[0]} />
        <img src={props.snapshots[1]} />
      </div>
    </div>
  )
}

export default CameraSnapshot;
