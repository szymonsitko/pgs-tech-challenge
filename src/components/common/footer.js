import React from 'react';

const Footer = () => {
  return (
    <div>
      <footer style={styles.footer} className="app-footer">
        Footer
      </footer>
    </div>
  );
}

const styles = {
  footer: {
    position: 'absolute',
    bottom: 0;
  }
}

export default Footer;
