import React, { Component } from 'react';
import CameraSnapshot from './camerasnapshot';
import { createDataStore } from '../../lib/createdatastore';
import * as constants from '../../constants';

class CamerasFrame extends Component {
  constructor(props) {
    super(props);

    this.state = {
      imagesDataStore: createDataStore(
        props.imagesData,
        constants.FIRST_CAMERA_LOCATION,
        constants.SECOND_CAMERA_LOCATION
      )
    }
  }

  render() {
    return (
      <div className="snapshot-frames">
        {this.state.imagesDataStore.map((item) => {
          return (
            <CameraSnapshot
              key={item.name}
              name={item.name}
              snapshots={
                [
                  item.cams[Object.keys(item.cams)[0]].url,
                  item.cams[Object.keys(item.cams)[1]].url
                ]
              }
            />
          );
        })
        }
      </div>
    );
  }
}

export default CamerasFrame;
