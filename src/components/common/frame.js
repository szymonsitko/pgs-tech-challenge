import React from 'react';
import ABOUT_US_FRAMES_DATA from '../../constants';

const Frame = props => {
  return (
    <div className="about-frame">
      <img src={props.uri} alt={props.title} />
        <div className="frame-container">
          <div className="frame-inner-container">
            <p className="frame-content-title">{props.title}</p>
            <p className="frame-content-text">{props.content}</p>
          </div>
        </div>
    </div>
  );
}

export default Frame;
