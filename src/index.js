// ReactJS standard boilerplate
import React from 'react';
import ReactDOM from 'react-dom';
// Custom components, made for the purpose of this particular project
import AppContainer from './containers/app_container';

ReactDOM.render(
  <AppContainer />
  , document.querySelector('.container')
);
