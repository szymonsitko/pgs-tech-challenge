import React, { Component } from 'react';
import axios from 'axios';
import Spinner from 'react-spinner';
import CamerasFrame from '../../components/common/camerasframe';
import * as constants from '../../constants';

class Cameras extends Component {
  constructor(props) {
    super(props);

    this.state = {
      imagesFetched: {},
      imagesFetchFailure: false
    }
  }

  componentWillMount() {
    const config = {
      headers: { 'X-Mashape-Key': constants.MASHAPE_API_KEY }
    };
    axios.get(constants.MASHAPE_URL, config).then(response => {
      this.setState({ imagesFetched: response.data });
    }).catch(error => {
      this.setState({ imagesFetchFailure: true }, function() {
        console.log(this.state.imagesFetchFailure)
      }.bind(this))
    })
  }

  renderCamerasFrame() {
    if (Object.keys(this.state.imagesFetched).length > 0) {
      return (
        <CamerasFrame
        imagesData={this.state.imagesFetched}
        fetchFailure={this.state.imagesFetchFailure}
        />
      );
    }
  }

  renderFailureMessage() {
    if (this.state.imagesFetchFailure) {
      return <h6>Cannot fetch the images from source. Try again later!</h6>;
    }
  }

  renderSpinner() {
    if (!this.state.imagesFetchFailure) {
      return (
        <div className="loading-indicator">
          <img src="../../../assets/images/spinner.gif" style={{ width: 62 }}/>
        </div>
      );
    }
  }

  render() {
    return (
      <div>
        {Object.keys(this.state.imagesFetched).length > 0 ? this.renderCamerasFrame() : this.renderSpinner()}
        {this.renderFailureMessage()}
      </div>
    )
  }
}

export default Cameras;
