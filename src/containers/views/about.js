import React, { Component } from 'react';
import Frames from '../../components/common/frames';
import Frame from '../../components/common/frame';
import * as constants from '../../constants';

class About extends Component {
  render() {
    return (
      <div>
        <Frames dataSource={constants.ABOUT_US_FRAMES_DATA}/>
      </div>
    )
  }
}

export default About;
