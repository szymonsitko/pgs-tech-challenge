import React, { Component } from 'react';
import NavBar from '../components/common/navbar';
import About from './views/about';
import Cameras from './views/cameras';
import Contact from './views/contact';

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isActiveComponent: 'about'
    }
  }

  onToggleComponent(componentId) {
    this.setState({ isActiveComponent: componentId });
  }

  renderViewComopnent() {
    if (this.state.isActiveComponent === 'about') {
      return <About { ...this.props }/>;
    }
    if (this.state.isActiveComponent === 'skicams') {
      return <Cameras { ...this.props } />;
    }
    if (this.state.isActiveComponent === 'contact') {
      return <Contact { ...this.props } />;
    }
  }

  render() {
    return (
      <div className="app-container">
        <NavBar onToggleComponent={this.onToggleComponent.bind(this)}/>
        <div className="main-container">
          {this.renderViewComopnent()}
        </div>
      </div>
    );
  }
}

export default App;
