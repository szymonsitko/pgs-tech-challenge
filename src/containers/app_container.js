import React, { Component } from 'react';
import App from './app';

class AppContainer extends Component {
  render() {
    return (
      <App />
    );
  }
}

export default AppContainer;
